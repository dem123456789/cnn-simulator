#! /usr/bin/python
import os


TEST_DIR = '../../test_files'
OUTPUT_DIR  = '../../test_outputs'
#GENES = ['ConcentricContour', 'ConnectedComponent', 'CornerDetection', 'EdgeDetection', 'HoleFilling', 'RotationDetector', 'ShadowCreator']
GENES=['ConnectedComponent']
TESTS = ['lowerA', 'capitalA', 'vertLine', 'circle', 'rect', 'filledSquare', 'zero', 'seven', 'eight', 'nine']
DIMS = [1024, 2048]


def gen_results():
    tests = []
    for dim in DIMS:
        for test_name in TESTS:
            tests.append('%s_%dx%d.dlm' % (test_name, dim, dim))

    num_tests = len(GENES) * len(tests)
    i = 0
    for gene in GENES:
        output_dir = os.path.join(OUTPUT_DIR, gene)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        for test_file in tests:
            print('%s - %s' % (gene, test_file))
            output_file = '%s_%s' % (gene, test_file)
            
            os.system('./run-cnn %s %s %s' % 
                        (gene, 
                         os.path.join(TEST_DIR, test_file),
                         os.path.join(output_dir, output_file)))
          
            i = i + 1
            print('Completed %d/%d tests...' % (i, num_tests))


if __name__ == "__main__":
    gen_results()
