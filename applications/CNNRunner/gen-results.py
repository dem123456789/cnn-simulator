#! /usr/bin/python
import os


#GENES = ["EdgeDetection", "CornerDetection", "ConnectedComponent", "ShadowCreator"]
GENES = ["HoleFilling"]
TEST_DIR = '../../tests'
OUTPUT_DIR  = '../../test_outputs'


def gen_results():
    tests = [x for x in os.listdir(TEST_DIR) if x.endswith('.dlm')]
    tests = [x for x in tests if not 'spiral' in x]
    num_tests = len(GENES) * len(tests)
    i = 0
    for gene in GENES:
        output_dir = os.path.join(OUTPUT_DIR, gene)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        for test_file in tests:
            print('%s - %s' % (gene, test_file))
            output_file = '%s_%s' % (gene, test_file)
            
            os.system('./run-cnn %s %s %s' % 
                        (gene, 
                         os.path.join(TEST_DIR, test_file),
                         os.path.join(output_dir, output_file)))
          
            i = i + 1
            print('Completed %d/%d tests...' % (i, num_tests))


if __name__ == "__main__":
    gen_results()
