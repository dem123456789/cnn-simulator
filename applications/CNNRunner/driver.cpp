#include <string>
#include <iostream>
#include "utilities/BWImage.h"
#include "cnn/cnn.h"
#include "cpu/CNNArrayCPU.h"

using namespace std;

int main(int argc, char* argv[])
{
  int num_args_needed = 4;
  if (argc != num_args_needed)
  {
    // Error on necessary parameters
    cerr << "Invalid number of arguments. Expected " << num_args_needed << " arguments. Received " << argc-1 << " arguments." << endl;
    return -1;
  }

  string geneName = string(argv[1]);
  string testInputFile = string(argv[2]);
  string outputFile = string(argv[3]);

  cout << "Creating CNN Gene " << geneName << "..." << endl; 
  CNNGene* gene = CNNGeneFactory::createCNNGene(geneName);

  cout << "Reading test input..." << endl;
  BWImage testIn, correctOutput;
  testIn.dlmread(testInputFile, " ");

  CNNInput* input = gene->getInput(testIn.getImage(), testIn.R(), testIn.C());
  CNNArrayCPU array(testIn.R(), testIn.C(), gene->r());
  array.convergenceRun(gene, input);
  BWImage output(testIn.R(), testIn.C());
  array.copyOutput(output.getImage());
  output.dlmwrite(outputFile, " ");

  delete gene;
  delete input;

  return 0;
}
