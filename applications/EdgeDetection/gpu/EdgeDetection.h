#ifndef EDGE_DETECTION_H
#define EDGE_DETECTION_H

#include "utilities/BWImage.h"

void EdgeDetection();
void EdgeDetectionSetup(BWImage* in, BWImage* out);
void EdgeDetectionClose();

#endif