#ifndef CORNER_DETECTION_H
#define CORNER_DETECTION_H

#include "utilities/BWImage.h"


void CornerDetection(BWImage* in, BWImage* out);

#endif // CORNER_DETECTION_H
