#include <string>
#include <iostream>

#include "timing/timer.hpp"

#include "ShadowCreator.h"

using namespace std;

#define NUM_TRIALS (10)

int main(int argc, char* argv[])
{
	int num_args_needed = 2;
	if (argc != num_args_needed)
	{
		// Error on necessary parameters
		cerr << "Invalid number of arguments. Expected " << num_args_needed << " arguments. Received " << argc << " arguments." << endl;
		return -1;
	}

	string testInputFile = string(argv[1]);
	BWImage testIn;
	testIn.dlmread(testInputFile, " ");
	BWImage testOut(testIn.R(), testIn.C());

	ShadowCreatorSetup(&testIn, &testOut);

	for (int i = 0; i < NUM_TRIALS; i++)
	{
    timer cnn_timer;
    ShadowCreator();
    double time_passed = cnn_timer.get_ms(); 
    cout << "Total Time (ms): " << time_passed << endl;
	}


	ShadowCreatorClose();
}
