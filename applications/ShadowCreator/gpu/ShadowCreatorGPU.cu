#include <iostream>

#include "ShadowCreator.h"
#include "cuda_utils.h"

#define BLOCKSIZE (128)

	/*
	 * Uses naive approach to parallelize problem of looping through all rows from right to left.
	 *  Works best for larger images because each warp is 32 threads, maximum warp occupancy is 64,
	 *  for a total of 32 * 64 = 2048 threads per multiprocessor, and there is more than 1 multiprocessor.
	 */
__global__ void ShadowCreatorGPU(const size_t rows, const size_t columns, float* imageIn, float* imageOut)
{
	int index_y = blockIdx.y * blockDim.y + threadIdx.y;

	if (index_y < rows)
	{
		int index_x = (int)(columns - 1);
		int value = -1;

		while (index_x >= 0) {
			int val2 = imageIn[index_y * rows + index_x];
			if ((value == -1) & (val2 == 1.0))
				value = 1.0;


			imageOut[index_y * rows + index_x] = -1;
			--index_x;
		}
		
	}
}

static float* h_imageIn;
static float* h_imageOut;
static float* d_imageIn;
static float* d_imageOut;
static size_t rows;
static size_t columns;
static dim3 blockSize;
static dim3 gridSize;

void ShadowCreatorSetup(BWImage* in, BWImage* out)
{
	h_imageIn = in->getImage();
	h_imageOut = out->getImage();

	rows = in->R();
	columns = in->C();
	//fprintf(stdout, "%ld-%ld\n", rows, columns);

	blockSize.x = 1; //BLOCKSIZE;
	blockSize.y = BLOCKSIZE;

	gridSize.x = 1; //(columns - 1) / blockSize.x + 1;
	gridSize.y = (rows - 1) / blockSize.y + 1;

	cudaMalloc((void**)&d_imageIn, sizeof(float) * rows * columns);
	cudaMalloc((void**)&d_imageOut, sizeof(float) * rows * columns);
	cudaMemcpy(d_imageIn, h_imageIn, sizeof(float) * rows * columns, cudaMemcpyHostToDevice);
	cudaMemcpy(d_imageOut, h_imageOut, sizeof(float) * rows * columns, cudaMemcpyHostToDevice);
}

void ShadowCreator()
{
	using namespace std;
	
	cudaEvent_t start, stop;
	CUDA_CHECK_ERROR (cudaEventCreate (&start));
	CUDA_CHECK_ERROR (cudaEventCreate (&stop));
	float elapsedTime;
	CUDA_CHECK_ERROR (cudaEventRecord (start, 0));

	ShadowCreatorGPU<<<gridSize, blockSize>>>(rows, columns, d_imageIn, d_imageOut);
	cudaThreadSynchronize();

        CUDA_CHECK_ERROR (cudaEventRecord (stop, 0));
	CUDA_CHECK_ERROR (cudaEventSynchronize (stop));
	CUDA_CHECK_ERROR (cudaEventElapsedTime (&elapsedTime, start, stop));
	elapsedTime = elapsedTime / 5;

	fprintf (stdout, "GPU Time (ms): %f\n", elapsedTime);
	cudaError_t code;
	if (cudaSuccess != (code = cudaDeviceSynchronize()))
	{
		cerr << "GPU ERROR: " << cudaGetErrorString(code) << " " << __FILE__ << " " << __LINE__ << endl;
		exit(code);
	}

	CUDA_CHECK_ERROR (cudaEventDestroy (start));
	CUDA_CHECK_ERROR (cudaEventDestroy (stop));
}

void ShadowCreatorClose()
{
	cudaMemcpy(h_imageOut, d_imageOut, sizeof(float) * rows * columns, cudaMemcpyDeviceToHost);

	cudaFree(d_imageIn);
	cudaFree(d_imageOut);
}
