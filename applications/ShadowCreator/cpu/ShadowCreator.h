#ifndef SHADOW_CREATOR_H
#define SHADOW_CREATOR_H

#include "utilities/BWImage.h"


void ShadowCreator(BWImage* in, BWImage* out);

#endif // SHADOW_CREATOR_H
