#include <string>
#include <iostream>

#include "timing/timer.hpp"

#include "ShadowCreator.h"

using namespace std;

#define NUM_TRIALS 10

void ShadowCreator(BWImage* in, BWImage* out) 
{
  float* img = in->getImage();
  float* img2 = out->getImage();
  size_t R = in->R();
  size_t C = in->C();

  for (size_t i = 0; i < R; i++)  {
    int j = C - 1;
    while ((img[i * R + j] != 1) && (j >= 0)) {
      img2[i * R + j] = -1;
      j--;
    }
    j--;
    for (; j >= 0; j--) {
      img2[i * R + j] = 1; 
    }
  }
}


int main(int argc, char* argv[])
{
  int num_args_needed = 2;
  if (argc != num_args_needed)
  {
    // Error on necessary parameters
    cerr << "Invalid number of arguments. Expected " << num_args_needed << " arguments. Received " << argc << " arguments." << endl;
    return -1;
  }

  string testInputFile = string(argv[1]);
  BWImage testIn;
  testIn.dlmread(testInputFile, " ");
  BWImage testOut(testIn.R(), testIn.C());

  for (int i = 0; i < NUM_TRIALS; i++) {
    timer cnn_timer;
    ShadowCreator(&testIn, &testOut);
    double time_passed = cnn_timer.get_ms(); 
    cout << "Total Time (ms): " << time_passed << endl;
  }
}
