#ifndef EDGE_DETECTION_H
#define EDGE_DETECTION_H

#include "utilities/BWImage.h"

void ConnectedComponent();
void ConnectedComponentSetup(BWImage* in, int* numberConnectedComponent);
void ConnectedComponentClose();

#endif