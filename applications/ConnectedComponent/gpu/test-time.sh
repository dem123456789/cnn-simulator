
declare -a dims=(64 128 256 512 1024 2048 4096)
test=circle
test_dir=../../../test_files

for dim in ${dims[@]} ; do
  echo $dim
  ./connected-component ${test_dir}/${test}_${dim}x${dim}.dlm
  echo ""
done
