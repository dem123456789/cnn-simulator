#include <string>
#include <iostream>

#include "timing/timer.hpp"

#include "ConnectedComponent.h"

using namespace std;

#define NUM_TRIALS 10

void ConnectedComponent(BWImage* in, int* num_comp_list) 
{
  float* img = in->getImage();
  size_t R = in->R();
  size_t C = in->C();

  for (size_t i = 0; i < R; i++) {
    float prevVal = img[i * R];
    int numComponents = 0;
    for (size_t j = 1; j < C; j++) {
      if (prevVal == -1 && img[i * R + j] == 1) {
        numComponents++;
      }
      prevVal = img[i * R + j];
    }
    num_comp_list[i] = numComponents;
  }
}


int main(int argc, char* argv[])
{
  int num_args_needed = 2;
  if (argc != num_args_needed)
  {
    // Error on necessary parameters
    cerr << "Invalid number of arguments. Expected " << num_args_needed << " arguments. Received " << argc << " arguments." << endl;
    return -1;
  }

  string testInputFile = string(argv[1]);
  BWImage testIn;
  testIn.dlmread(testInputFile, " ");

  int* num_comp_list = new int[testIn.R()];
  double time_passed = 0;
  for (int i = 0; i < NUM_TRIALS; i++) {
    timer cnn_timer;
    ConnectedComponent(&testIn, num_comp_list);
    double time_passed = cnn_timer.get_ms(); 
    cout << "Total Time (ms): " << time_passed << endl;
  }

  cout << "Average Time (ms): " << time_passed / NUM_TRIALS << endl;
  
  delete[] num_comp_list;
}
