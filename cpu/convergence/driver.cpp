#include <iostream>
#include "cpu/CNNArrayCPU.h"
#include "utilities/BWImage.h"
#include "cnn/cnn.h"

using namespace std;

int main(int argc, char* argv[])
{
  int num_args_needed = 3;
  if (argc != num_args_needed)
  {
    // Error on necessary parameters
    cerr << "Invalid number of arguments. Expected " << num_args_needed << " arguments. Received " << argc << " arguments." << endl;
    return -1;
  }

  string geneName = string(argv[1]);
  string testInputFile = string(argv[2]);
  float dt = 1.0;
  if (argc == 4)
    dt = atof(argv[3]);

  BWImage testIn;
  testIn.dlmread(testInputFile, " ");

  CNNGene* gene = CNNGeneFactory::createCNNGene(geneName);
  CNNInput* input = gene->getInput(testIn.getImage(), testIn.R(), testIn.C());
  
  CNNArrayCPU array(testIn.R(), testIn.C(), gene->r());
  unsigned int digitalTime = array.convergenceRun(gene, input);

  cout << geneName << " (" << testInputFile << "): " << digitalTime << endl;
  
  delete gene;
  delete input;

  return 0;
}
