
declare -a genes=("ConcentricContour" "ConnectedComponent" "CornerDetection" "EdgeDetection" "HoleFilling" "RotationDetector" "ShadowCreator")
declare -a test_files=("lowerA" "capitalA" "vertLine" "circle" "rect" "filledSquare" "zero" "seven" "eight" "nine")
declare -a dims=(1024 2048)

for dim in ${dims[@]} ; do
  for gene in ${genes[@]} ; do
    for test in ${test_files[@]} ; do
      ./cnn-converge ${gene} ../../test_files/${test}_${dim}x${dim}.dlm
    done
  done
done
