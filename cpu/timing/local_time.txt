Analog v1
64
Time (ms): 49.9746
Time (ms): 51.4281
Time (ms): 52.4785
Time (ms): 52.3411
Time (ms): 48.9476

128
Time (ms): 194.878
Time (ms): 194.164
Time (ms): 194.083
Time (ms): 194.375
Time (ms): 194.273

256
Time (ms): 1202.27
Time (ms): 1201.95
Time (ms): 1201.89
Time (ms): 1202.28
Time (ms): 1204.18

512
Time (ms): 4832.56
Time (ms): 4808.99
Time (ms): 4811.16
Time (ms): 4811.34
Time (ms): 4808.48

1024
Time (ms): 21247.1
Time (ms): 21262.3
Time (ms): 21239.2
Time (ms): 21239.5
Time (ms): 21293.6

2048
Time (ms): 85575.8
Time (ms): 85653.4
Time (ms): 85549.2
Time (ms): 85572.6
Time (ms): 86091.8

4096
Time (ms): 342613
Time (ms): 342480
Time (ms): 342569
Time (ms): 342997
Time (ms): 342561

Difference Equation v1
64
Time (ms): 45.5553
Time (ms): 45.9322
Time (ms): 47.5492
Time (ms): 45.5225
Time (ms): 49.7622

128
Time (ms): 180.063
Time (ms): 179.729
Time (ms): 179.54
Time (ms): 184.921
Time (ms): 184.19

256
Time (ms): 1083.26
Time (ms): 1066.18
Time (ms): 1065.35
Time (ms): 1068.66
Time (ms): 1068.54

512
Time (ms): 4272.39
Time (ms): 4348.38
Time (ms): 4316.39
Time (ms): 4318.06
Time (ms): 4270.72

1024
Time (ms): 19032
Time (ms): 19051.3
Time (ms): 19034.1
Time (ms): 19043.8
Time (ms): 19039.1

2048
Time (ms): 76468.6
Time (ms): 76489.5
Time (ms): 76566
Time (ms): 76491.4
Time (ms): 76478

4096
Time (ms): 306648
Time (ms): 305965
Time (ms): 305772
Time (ms): 306067
Time (ms): 306582

Analog v2
64
Time (ms): 65.7091
Time (ms): 72.3223
Time (ms): 68.3027
Time (ms): 71.3579
Time (ms): 64.4578

128
Time (ms): 256.174
Time (ms): 255.719
Time (ms): 256.032
Time (ms): 255.614
Time (ms): 256

256
Time (ms): 1186.18
Time (ms): 1188.04
Time (ms): 1181.72
Time (ms): 1194.24
Time (ms): 1181.89

512
Time (ms): 4748.82
Time (ms): 4762.89
Time (ms): 4760.56
Time (ms): 4751.74
Time (ms): 4756.04

1024
Time (ms): 20966.6
Time (ms): 20922.4
Time (ms): 20962.1
Time (ms): 20983.7
Time (ms): 21015.6

2048
Time (ms): 83780.3
Time (ms): 83681.1
Time (ms): 83875.4
Time (ms): 83844.9
Time (ms): 83835.8

4096
Time (ms): 334153
Time (ms): 334635
Time (ms): 334374
Time (ms): 335351
Time (ms): 335143

Difference Equation v2
64
Time (ms): 63.8694
Time (ms): 64.1365
Time (ms): 62.4763
Time (ms): 59.1801
Time (ms): 62.15

128
Time (ms): 231.29
Time (ms): 231.282
Time (ms): 231.54
Time (ms): 231.44
Time (ms): 231.251

256
Time (ms): 1430.86
Time (ms): 1431.7
Time (ms): 1430.36
Time (ms): 1431.14
Time (ms): 1430.03

512
Time (ms): 5725.76
Time (ms): 5725.69
Time (ms): 5724.21
Time (ms): 5726.65
Time (ms): 5726.86

1024
Time (ms): 24704.4
Time (ms): 24688.5
Time (ms): 24886.5
Time (ms): 24734.9
Time (ms): 24690.2

2048
Time (ms): 98757.8
Time (ms): 98585.5
Time (ms): 98740.6
Time (ms): 98786.3
Time (ms): 98573.1

4096
Time (ms): 395349
Time (ms): 394358
Time (ms): 394350
Time (ms): 394402
Time (ms): 394886

