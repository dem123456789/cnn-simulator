#include "CNNArrayOpenMPSIMD.h"
#include <cmath>
#include <string.h>
#include <assert.h>

#include "hpcdefs.hpp"

using namespace std;
CNNArrayOpenMPSIMD::CNNArrayOpenMPSIMD(size_t M, size_t N, size_t r)
{
  _M = M;
  _N = N;
  _r = r;

  _R = M + 2 * r;
  _C = N + 2 * r;

  _state = new float[_R * _C];
  _output = new float[_R * _C];
}

CNNArrayOpenMPSIMD::~CNNArrayOpenMPSIMD() 
{
  delete[] _state;
  delete[] _output;
}

void CNNArrayOpenMPSIMD::run(CNNGene* gene, CNNInput* input, unsigned int N, float dt)
{
  assert(gene->r() == _r);
  assert(input->R() == _R);
  assert(input->C() == _C);

  _gene = gene;
  _input = input;
 
  // Copy the initial state over 
  memcpy(_state, input->initialState(), sizeof(float) * _R * _C); 
  this->computeOutput();
  for (unsigned int i = 1; i <= N; i++) {
    this->computeNextState(dt);
    this->computeOutput();
  }
}

void CNNArrayOpenMPSIMD::computeNextState(const float dt)
{
  const float z = _gene->z();
  const float* A  = _gene->A();
  const float* B = _gene->B();
  const float* u = _input->u();
  static const __m128 zVec = _mm_set1_ps(z);
  static const __m128 dtVec = _mm_set1_ps(dt);

  size_t M = _M;
  size_t N = _N;
  size_t C = _C;
  size_t r = _r;
  float* state = _state;
  float* output = _output;

#pragma omp parallel for default(none) shared(A, B, u, M, N, r, C, state, output)
  for (size_t i = 0; i < M; ++i) {
    size_t j;
    for (j = 0; (j + 4) <= N; j += 4) {
      __m128 states = _mm_loadu_ps(&(_state[(i + r) * C + (j + r)]));
      __m128 derivs = zVec; 

      for (size_t k = 0; k < (2 * r + 1); k++) {
        for (size_t l = 0; l < (2 * r + 1); l++) {
          __m128 Afactor = _mm_load1_ps(&A[k * (2 * r + 1) + l]);
          __m128 outVals = _mm_loadu_ps(&(output[(i + k) * C + (j + l)]));

          __m128 Bfactor = _mm_load1_ps(&B[k * (2 * r + 1) + l]);
          __m128 uVals = _mm_loadu_ps(&(u[(i + k) * C + (j + l)]));

          derivs = _mm_add_ps(derivs, _mm_mul_ps(Afactor, outVals));
          derivs = _mm_add_ps(derivs, _mm_mul_ps(Bfactor, uVals));
        }
      }

      derivs = _mm_sub_ps(derivs, states);
      __m128 delta = _mm_mul_ps(derivs, dtVec);
      __m128 newStates = _mm_add_ps(states, delta);
      _mm_storeu_ps(&(state[(i + r) * C + (j + r)]), newStates);
    }


    for (; j < N; ++j) {
      float stateVal = _state[(i + r) * C + (j + r)];
      
      float stateDeriv = -stateVal + z;
      for (size_t k = 0; k < (2 * r + 1); k++) {
        for (size_t l = 0; l < (2 * r + 1); l++) {
          stateDeriv += A[k * (2 * r + 1) + l] * output[(i + k) * C + (j + l)];
          stateDeriv += B[k * (2 * r + 1) + l] * u[(i + k) * C + (j + l)];
        }
      }

      state[(i + r) * _C + (j + _r)] = stateVal + stateDeriv * dt;
    }
  }
}

void CNNArrayOpenMPSIMD::run(CNNGene* gene, CNNInput* input, unsigned int N)
{
  assert(gene->r() == _r);
  assert(input->R() == _R);
  assert(input->C() == _C);

  _gene = gene;
  _input = input;
 
  // Copy the initial state over 
  memcpy(_state, input->initialState(), sizeof(float) * _R * _C); 
  this->computeOutput();
  for (unsigned int i = 1; i <= N; i++) {
    this->computeNextState();
    this->computeOutput();
  }
}

void CNNArrayOpenMPSIMD::computeNextState()
{
  const float z = _gene->z();
  const float* A  = _gene->A();
  const float* B  = _gene->B();
  const float* u  = _input->u();

  static const __m128 zVec = _mm_set1_ps(z);
  
  size_t M = _M;
  size_t N = _N;
  size_t C = _C;
  size_t r = _r;
  float* state = _state;
  float* output = _output;

#pragma omp parallel for default(none) shared(A, B, u, M, N, r, C, state, output)
  for (size_t i = 0; i < M; ++i) {
    size_t j;
    for (j = 0; (j + 4) <= N; j += 4) {
      __m128 newStates = zVec; 

      for (size_t k = 0; k < (2 * r + 1); k++) {
        for (size_t l = 0; l < (2 * r + 1); l++) {
          __m128 Afactor = _mm_load1_ps(&A[k * (2 * r + 1) + l]);
          __m128 outVals = _mm_loadu_ps(&(output[(i + k) * C + (j + l)]));

          __m128 Bfactor = _mm_load1_ps(&B[k * (2 * r + 1) + l]);
          __m128 uVals = _mm_loadu_ps(&(u[(i + k) * C + (j + l)]));

          newStates = _mm_add_ps(newStates, _mm_mul_ps(Afactor, outVals));
          newStates = _mm_add_ps(newStates, _mm_mul_ps(Bfactor, uVals));
        }
      }

      _mm_storeu_ps(&(_state[(i + r) * _C + (j + r)]), newStates);
    }


    for (; j < N; ++j) {
      float newState = z;
      for (size_t k = 0; k < (2 * r + 1); k++) {
        for (size_t l = 0; l < (2 * r + 1); l++) {
          newState += A[k * (2 * r + 1) + l] * output[(i + k) * C + (j + l)];
          newState += B[k * (2 * r + 1) + l] * u[(i + k) * C + (j + l)];
        }
      }

      state[(i + r) * C + (j + r)] = newState;
    }
  }
}

void CNNArrayOpenMPSIMD::computeInitialOutput()
{
  static const __m128 oneVec = _mm_set1_ps(1.0f);
  static const __m128 twoVec = _mm_set1_ps(2.0f);
  static const __m128 SIGNMASK = 
                   _mm_castsi128_ps(_mm_set1_epi32(0x7FFFFFFF));

  size_t R = _R;
  size_t C = _C;
  float* state = _state;
  float* output = _output;
#pragma omp parallel for default(none) shared(R, C, state, output)
  for (size_t i = 0; i < R; ++i) {
    size_t j;
    for (j = 0; (j + 4) <= C; j += 4) {
      __m128 states = _mm_loadu_ps(&(state[i *_C + j]));
      __m128 part1 = _mm_add_ps(states, oneVec);
      part1 = _mm_and_ps(SIGNMASK, part1);

      __m128 part2 = _mm_sub_ps(states, oneVec);
      part2 = _mm_and_ps(SIGNMASK, part2);

      __m128 outputVals = _mm_sub_ps(part1, part2);
      outputVals = _mm_div_ps(outputVals, twoVec);

      _mm_storeu_ps(&(output[i * C + j]), outputVals);
    }

    for (; j < C; ++j) {
      float stateVal = state[i * C + j];
      output[i * C + j] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
    }
  }
}

void CNNArrayOpenMPSIMD::computeOutput()
{
  static const __m128 oneVec = _mm_set1_ps(1.0f);
  static const __m128 twoVec = _mm_set1_ps(2.0f);
  static const __m128 SIGNMASK = 
                   _mm_castsi128_ps(_mm_set1_epi32(0x7FFFFFFF));

  for (size_t i = 0; i < _M; ++i) {
    size_t j;
    for (size_t j = 0; (j + 4) <= _N; j += 4) {
      __m128 states = _mm_loadu_ps(&(_state[(i + _r) * _C + (j + _r)]));
      __m128 part1 = _mm_add_ps(states, oneVec);
      part1 = _mm_and_ps(SIGNMASK, part1);

      __m128 part2 = _mm_sub_ps(states, oneVec);
      part2 = _mm_and_ps(SIGNMASK, part2);

      __m128 outputVals = _mm_sub_ps(part1, part2);
      outputVals = _mm_div_ps(outputVals, twoVec);

      _mm_storeu_ps(&(_output[(i + _r) * _C + (j + _r)]), outputVals);
    }

    for (; j < _N; ++j) {
      float stateVal = _state[(i + _r) * _C + (j + _r)];
      _output[(i + _r) * _C + (j + _r)] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
    }
  }
}

void CNNArrayOpenMPSIMD::copyOutput(float* out)
{
  for (size_t i = 0; i < _M; ++i) {
    for (size_t j = 0; j < _N; ++j) {
      out[i* _N + j] = _output[(i + _r) * _C + (j + _r)];
    }
  }
}


////////////////////////////////////////////////////////////////////
// CNNArrayOpenMPSIMD - Increased Memory Usage Version
////////////////////////////////////////////////////////////////////
#include <iostream>
CNNArrayOpenMPSIMDv2::CNNArrayOpenMPSIMDv2(size_t M, size_t N, size_t r)
{
  _M = M;
  _N = N;
  _r = r;

  _R = M + 2 * r;
  _C = N + 2 * r;

  _state = new float[_R * _C];
  _output = new float[_R * _C];
  _tmp_output = new float[_R * _C];
}

CNNArrayOpenMPSIMDv2::~CNNArrayOpenMPSIMDv2() 
{
  delete[] _state;
  delete[] _output;
  delete[] _tmp_output;
}

void CNNArrayOpenMPSIMDv2::run(CNNGene* gene, CNNInput* input, unsigned int N, float dt)
{
  assert(gene->r() == _r);
  assert(input->R() == _R);
  assert(input->C() == _C);

  _gene = gene;
  _input = input;
 
  // Copy the initial state over 
  memcpy(_state, input->initialState(), sizeof(float) * _R * _C); 
  this->computeInitialOutput();
  for (unsigned int i = 1; i <= N; i++) {
    this->computeStateAndOutput(dt);
    float* tmp = _output;
    _output = _tmp_output;
    _tmp_output = tmp;
  }
}

void CNNArrayOpenMPSIMDv2::run(CNNGene* gene, CNNInput* input, unsigned int N)
{
  assert(gene->r() == _r);
  assert(input->R() == _R);
  assert(input->C() == _C);

  _gene = gene;
  _input = input;
 
  // Copy the initial state over 
  memcpy(_output, input->initialState(), sizeof(float) * _R * _C); 
  this->computeInitialOutput2();
  for (unsigned int i = 1; i <= N; i++) {
    this->computeStateAndOutput();
    float* tmp = _output;
    _output = _tmp_output;
    _tmp_output = tmp;
  }
}

void CNNArrayOpenMPSIMDv2::computeStateAndOutput(const float dt)
{
  const float z = _gene->z();
  const float* A  = _gene->A();
  const float* B  = _gene->B();
  const float* u  = _input->u();

  static const __m128 zVec = _mm_set1_ps(z);
  static const __m128 dtVec = _mm_set1_ps(dt);
  static const __m128 oneVec = _mm_set1_ps(1.0f);
  static const __m128 twoVec = _mm_set1_ps(2.0f);
  static const __m128 SIGNMASK = 
                   _mm_castsi128_ps(_mm_set1_epi32(0x7fffffff));

  size_t M = _M;
  size_t N = _N;
  size_t C = _C;
  size_t r = _r;
  float* state = _state;
  float* output = _output;
  float* tmp_output = _tmp_output;
#pragma omp parallel for default(none) shared(A, B, u, M, N, r, C, state, output, tmp_output)
  for (size_t i = 0; i < M; ++i) {
    size_t j;
    for (j = 0; (j + 4) <=_N; j += 4) {
      __m128 states = _mm_loadu_ps(&(state[(i + r) * C + (j + r)]));
      __m128 derivs = zVec; 

      for (size_t k = 0; k < (2 * r + 1); k++) {
        for (size_t l = 0; l < (2 * r + 1); l++) {
          __m128 Afactor = _mm_load1_ps(&A[k * (2 * r + 1) + l]);
          __m128 outVals = _mm_loadu_ps(&(output[(i + k) * C + (j + l)]));

          __m128 Bfactor = _mm_load1_ps(&B[k * (2 * r + 1) + l]);
          __m128 uVals = _mm_loadu_ps(&(u[(i + k) * C + (j + l)]));

          derivs = _mm_add_ps(derivs, _mm_mul_ps(Afactor, outVals));
          derivs = _mm_add_ps(derivs, _mm_mul_ps(Bfactor, uVals));
        }
      }

      derivs = _mm_sub_ps(derivs, states);
      __m128 delta = _mm_mul_ps(derivs, dtVec);
      __m128 newStates = _mm_add_ps(states, delta);
      _mm_storeu_ps(&(state[(i + r) * C + (j + r)]), newStates);
      
      __m128 part1 = _mm_add_ps(newStates, oneVec);
      part1 = _mm_and_ps(SIGNMASK, part1);

      __m128 part2 = _mm_sub_ps(newStates, oneVec);
      part2 = _mm_and_ps(SIGNMASK, part2);

      __m128 outputVals = _mm_sub_ps(part1, part2);
      outputVals = _mm_div_ps(outputVals, twoVec);

      _mm_storeu_ps(&(_tmp_output[(i + r) * C + (j + r)]), outputVals);
    }


    for (; j < N; ++j) {
      float stateVal = state[(i + r) * C + (j + r)];
      
      float stateDeriv = -stateVal + z;
      for (size_t k = 0; k < (2 * r + 1); k++) {
        for (size_t l = 0; l < (2 * r + 1); l++) {
          stateDeriv += A[k * (2 * r + 1) + l] * output[(i + k) * C + (j + l)];
          stateDeriv += B[k * (2 * r + 1) + l] * u[(i + k) * C + (j + l)];
        }
      }

      stateVal += stateDeriv * dt;
      state[(i + r) * C + (j + r)] = stateVal;
      tmp_output[(i + r) * C + (j + r)] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
    }
  }
}


void CNNArrayOpenMPSIMDv2::computeStateAndOutput()
{
  const float z = _gene->z();
  const float* A  = _gene->A();
  const float* B  = _gene->B();
  const float* u  = _input->u();

  static const __m128 zVec = _mm_set1_ps(z);
  static const __m128 oneVec = _mm_set1_ps(1.0f);
  static const __m128 twoVec = _mm_set1_ps(2.0f);
  static const __m128 SIGNMASK = 
                   _mm_castsi128_ps(_mm_set1_epi32(0x7fffffff));

  size_t M = _M;
  size_t N = _N;
  size_t C = _C;
  size_t r = _r;
  float* output = _output;
  float* tmp_output = _tmp_output;
#pragma omp parallel for default(none) shared(A, B, u, M, N, r, C, output, tmp_output)
  for (size_t i = 0; i < M; ++i) {
    size_t j;
    for (j = 0; (j + 4) <= N; j += 4) {
      __m128 newStates = zVec; 

      for (size_t k = 0; k < (2 * r + 1); k++) {
        for (size_t l = 0; l < (2 * r + 1); l++) {
          __m128 Afactor = _mm_load1_ps(&A[k * (2 * r + 1) + l]);
          __m128 outVals = _mm_loadu_ps(&(output[(i + k) * C + (j + l)]));

          __m128 Bfactor = _mm_load1_ps(&B[k * (2 * r + 1) + l]);
          __m128 uVals = _mm_loadu_ps(&(u[(i + k) * C + (j + l)]));

          newStates = _mm_add_ps(newStates, _mm_mul_ps(Afactor, outVals));
          newStates = _mm_add_ps(newStates, _mm_mul_ps(Bfactor, uVals));
        }
      }

      _mm_storeu_ps(&(_state[(i + r) * C + (j + r)]), newStates);

      __m128 part1 = _mm_add_ps(newStates, oneVec);
      part1 = _mm_and_ps(SIGNMASK, part1);

      __m128 part2 = _mm_sub_ps(newStates, oneVec);
      part2 = _mm_and_ps(SIGNMASK, part2);

      __m128 outputVals = _mm_sub_ps(part1, part2);
      outputVals = _mm_div_ps(outputVals, twoVec);

      _mm_storeu_ps(&(_tmp_output[(i + r) * C + (j + r)]), outputVals);
    }


    for (; j < N; ++j) {
      float newState = z;
      for (size_t k = 0; k < (2 * r + 1); k++) {
        for (size_t l = 0; l < (2 * r + 1); l++) {
          newState += A[k * (2 * r + 1) + l] * output[(i + k) * C + (j + l)];
          newState += B[k * (2 * r + 1) + l] * u[(i + k) * C + (j + l)];
        }
      }

      tmp_output[(i + r) * C + (j + r)] = (abs(newState + 1) - abs(newState - 1)) / 2.0f;
    }
  }
}

void CNNArrayOpenMPSIMDv2::computeInitialOutput()
{
  static const __m128 oneVec = _mm_set1_ps(1.0f);
  static const __m128 twoVec = _mm_set1_ps(2.0f);
  static const __m128 SIGNMASK = 
                   _mm_castsi128_ps(_mm_set1_epi32(0x7FFFFFFF));

  size_t R = _R;
  size_t C = _C;
  float* state = _state;
  float* output = _output;
#pragma omp parallel for default(none) shared(R, C, state, output)
  for (size_t i = 0; i < R; ++i) {
    size_t j;
    for (j = 0; (j + 4) <= C; j += 4) {
      __m128 states = _mm_loadu_ps(&(state[i *_C + j]));
      __m128 part1 = _mm_add_ps(states, oneVec);
      part1 = _mm_and_ps(SIGNMASK, part1);

      __m128 part2 = _mm_sub_ps(states, oneVec);
      part2 = _mm_and_ps(SIGNMASK, part2);

      __m128 outputVals = _mm_sub_ps(part1, part2);
      outputVals = _mm_div_ps(outputVals, twoVec);

      _mm_storeu_ps(&(output[i * C + j]), outputVals);
    }

    for (; j < C; ++j) {
      float stateVal = state[i * C + j];
      output[i * C + j] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
    }
  }
}

void CNNArrayOpenMPSIMDv2::computeInitialOutput2()
{
  static const __m128 oneVec = _mm_set1_ps(1.0f);
  static const __m128 twoVec = _mm_set1_ps(2.0f);
  static const __m128 SIGNMASK = 
                   _mm_castsi128_ps(_mm_set1_epi32(0x7FFFFFFF));

  size_t R = _R;
  size_t C = _C;
  float* output = _output;
#pragma omp parallel for default(none) shared(R, C, output)
  for (size_t i = 0; i < R; ++i) {
    size_t j;
    for (j = 0; (j + 4) <= C; j += 4) {
      __m128 states = _mm_loadu_ps(&(output[i *_C + j]));
      __m128 part1 = _mm_add_ps(states, oneVec);
      part1 = _mm_and_ps(SIGNMASK, part1);

      __m128 part2 = _mm_sub_ps(states, oneVec);
      part2 = _mm_and_ps(SIGNMASK, part2);

      __m128 outputVals = _mm_sub_ps(part1, part2);
      outputVals = _mm_div_ps(outputVals, twoVec);

      _mm_storeu_ps(&(output[i * C + j]), outputVals);
    }

    for (; j < C; ++j) {
      float stateVal = output[i * C + j];
      output[i * C + j] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
    }
  }
}

void CNNArrayOpenMPSIMDv2::copyOutput(float* out)
{
  for (size_t i = 0; i < _M; ++i) {
    for (size_t j = 0; j < _N; ++j) {
      out[i* _N + j] = _output[(i + _r) * _C + (j + _r)];
    }
  }
}
