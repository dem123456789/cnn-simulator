#include "CNNArrayOpenMP.h"
#include <cmath>
#include <string.h>
#include <assert.h>

#include <iostream>
using namespace std;
CNNArrayOpenMP::CNNArrayOpenMP(size_t M, size_t N, size_t r)
{
  _M = M;
  _N = N;
  _r = r;

  _R = M + 2 * r;
  _C = N + 2 * r;

  _state = new float[_R * _C];
  _output = new float[_R * _C];
}

CNNArrayOpenMP::~CNNArrayOpenMP() 
{
  delete[] _state;
  delete[] _output;
}

void CNNArrayOpenMP::run(CNNGene* gene, CNNInput* input, unsigned int N, float dt)
{
  assert(gene->r() == _r);
  assert(input->R() == _R);
  assert(input->C() == _C);

  _gene = gene;
  _input = input;
 
  // Copy the initial state over 
  memcpy(_state, input->initialState(), sizeof(float) * _R * _C); 

  this->computeInitialOutput();
  for (unsigned int i = 1; i <= N; i++) {
    this->computeNextState(dt);
    this->computeOutput();
  }
}

void CNNArrayOpenMP::computeNextState(const float dt)
{
  const float z = _gene->z();
  const float* A  = _gene->A();
  const float* B = _gene->B();
  const float* u = _input->u();

  float stateVal, stateDeriv;
  size_t M = _M;
  size_t N = _N;
  size_t C = _C;
  size_t r = _r;
  float* output = _output;

#pragma omp parallel for default(none) shared(A, B, u, M, N, r, C, output) private(stateVal, stateDeriv)
  for (size_t i = 0; i < M; ++i) {
    for (size_t j = 0; j < N; ++j) {
      stateVal = _state[(i + r) * C + (j + r)];
      
      stateDeriv = -stateVal + z;
      for (size_t k = 0; k < (2 * r + 1); k++) {
        for (size_t l = 0; l < (2 * r + 1); l++) {
          stateDeriv += A[k * (2 * r + 1) + l] * output[(i + k) * C + (j + l)];
          stateDeriv += B[k * (2 * r + 1) + l] * u[(i + k) * C + (j + l)];
        }
      }

      _state[(i + r) * C + (j + r)] = stateVal + stateDeriv * dt;
    }
  }
}

void CNNArrayOpenMP::run(CNNGene* gene, CNNInput* input, unsigned int N)
{
  assert(gene->r() == _r);
  assert(input->R() == _R);
  assert(input->C() == _C);

  _gene = gene;
  _input = input;
 
  // Copy the initial state over 
  memcpy(_state, input->initialState(), sizeof(float) * _R * _C); 

  this->computeInitialOutput();
  for (unsigned int i = 1; i <= N; i++) {
    this->computeNextState();
    this->computeOutput();
  }
}

void CNNArrayOpenMP::computeNextState()
{
  const float z = _gene->z();
  const float* A  = _gene->A();
  const float* B = _gene->B();
  const float* u = _input->u();

  float delta;
  size_t M = _M;
  size_t N = _N;
  size_t C = _C;
  size_t r = _r;
  float* output = _output;

#pragma omp parallel for default(none) shared(A, B, u, M, N, r, C, output) private(delta)
  for (size_t i = 0; i < M; ++i) {
    for (size_t j = 0; j < N; ++j) {
      delta = z;
      for (size_t k = 0; k < (2 * r + 1); k++) {
        for (size_t l = 0; l < (2 * r + 1); l++) {
          delta += A[k * (2 * r + 1) + l] * output[(i + k) * C + (j + l)];
          delta += B[k * (2 * r + 1) + l] * u[(i + k) * C + (j + l)];
        }
      }

      _state[(i + r) * C + (j + r)] = delta;
    }
  }
}

void CNNArrayOpenMP::computeInitialOutput()
{
  float stateVal;
  size_t R = _R;
  size_t C = _C;
  float* output = _output;
#pragma omp parallel for default(none) shared(R, C, output) private(stateVal)
  for (size_t i = 0; i < R; ++i) {
    for (size_t j = 0; j < C; ++j) {
      stateVal = _state[i * C + j];
      output[i * C + j] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
    }
  }
}

void CNNArrayOpenMP::computeOutput()
{
  float stateVal;
  size_t M = _M;
  size_t N = _N;
  size_t C = _C;
  size_t r = _r;
  float* output = _output;
#pragma omp parallel for default(none) shared(M, N, r, C, output) private(stateVal)
  for (size_t i = 0; i < M; ++i) {
    for (size_t j = 0; j < N; ++j) {
      stateVal = _state[(i + r) * C + (j + r)];
      output[(i + r) * C + (j + r)] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
    }
  }
}

void CNNArrayOpenMP::copyOutput(float* out)
{
  for (size_t i = 0; i < _M; ++i) {
    for (size_t j = 0; j < _N; ++j) {
      out[i* _N + j] = _output[(i + _r) * _C + (j + _r)];
    }
  }
}

////////////////////////////////////////////////////////////////////
// CNNArrayOpenMP - Increased Memory Usage Version
////////////////////////////////////////////////////////////////////
CNNArrayOpenMPv2::CNNArrayOpenMPv2(size_t M, size_t N, size_t r)
{
  _M = M;
  _N = N;
  _r = r;

  _R = M + 2 * r;
  _C = N + 2 * r;

  _state = new float[_R * _C];
  _output = new float[_R * _C];
  _tmp_output = new float[_R * _C];
}

CNNArrayOpenMPv2::~CNNArrayOpenMPv2() 
{
  delete[] _state;
  delete[] _output;
  delete[] _tmp_output;
}

void CNNArrayOpenMPv2::run(CNNGene* gene, CNNInput* input, unsigned int N, float dt)
{
  assert(gene->r() == _r);
  assert(input->R() == _R);
  assert(input->C() == _C);

  _gene = gene;
  _input = input;
 
  // Copy the initial state over 
  memcpy(_state, input->initialState(), sizeof(float) * _R * _C); 
  this->computeInitialOutput();
  for (unsigned int i = 1; i <= N; i++) {
    this->computeStateAndOutput(dt);
    float* tmp = _output;
    _output = _tmp_output;
    _tmp_output = tmp;
  }
}

void CNNArrayOpenMPv2::run(CNNGene* gene, CNNInput* input, unsigned int N)
{
  assert(gene->r() == _r);
  assert(input->R() == _R);
  assert(input->C() == _C);

  _gene = gene;
  _input = input;
 
  // Copy the initial state over 
  memcpy(_output, input->initialState(), sizeof(float) * _R * _C); 
  this->computeInitialOutput2();
  for (unsigned int i = 1; i <= N; i++) {
    this->computeStateAndOutput();
    float* tmp = _output;
    _output = _tmp_output;
    _tmp_output = tmp;
  }
}

void CNNArrayOpenMPv2::computeStateAndOutput(const float dt)
{
  const float z = _gene->z();
  const float* A  = _gene->A();
  const float* B = _gene->B();
  const float* u = _input->u();

  float stateVal, stateDeriv;
  size_t M = _M;
  size_t N = _N;
  size_t C = _C;
  size_t r = _r;
  float* output = _output;
  float* tmp_output = _tmp_output;

#pragma omp parallel for default(none) shared(A, B, u, M, N, r, C, output, tmp_output) private(stateVal, stateDeriv)
  for (size_t i = 0; i < M; ++i) {
    for (size_t j = 0; j < N; ++j) {
      stateVal = _state[(i + r) * C + (j + r)];
      
      stateDeriv = -stateVal + z;
      for (size_t k = 0; k < (2 * r + 1); k++) {
        for (size_t l = 0; l < (2 * r + 1); l++) {
          stateDeriv += A[k * (2 * r + 1) + l] * output[(i + k) * C + (j + l)];
          stateDeriv += B[k * (2 * r + 1) + l] * u[(i + k) * C + (j + l)];
        }
      }

      stateVal += stateDeriv * dt;
      _state[(i + _r) * _C + (j + _r)] = stateVal;
      _tmp_output[(i + _r) * _C + (j + _r)] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
    }
  }
}


void CNNArrayOpenMPv2::computeStateAndOutput()
{
  const float z = _gene->z();
  const float* A  = _gene->A();
  const float* B = _gene->B();
  const float* u = _input->u();

  float delta;
  size_t M = _M;
  size_t N = _N;
  size_t C = _C;
  size_t r = _r;
  float* output = _output;
  float* tmp_output = _tmp_output;
#pragma omp parallel for default(none) shared(A, B, u, M, N, r, C, output, tmp_output) private(delta)
  for (size_t i = 0; i < M; ++i) {
    for (size_t j = 0; j < N; ++j) {
      delta = z;
      for (size_t k = 0; k < (2 * r + 1); k++) {
        for (size_t l = 0; l < (2 * r + 1); l++) {
          delta += A[k * (2 * r + 1) + l] * output[(i + k) * C + (j + l)];
          delta += B[k * (2 * r + 1) + l] * u[(i + k) * C + (j + l)];
        }
      }

      tmp_output[(i + _r) * _C + (j + _r)] = (abs(delta + 1) - abs(delta - 1)) / 2.0f;
    }
  }
}

void CNNArrayOpenMPv2::computeInitialOutput()
{
  float stateVal;
  float stateVal;
  size_t R = _R;
  size_t C = _C;
  float* output = _output;
#pragma omp parallel for default(none) shared(R, C, output) private(stateVal)
  for (size_t i = 0; i < R; ++i) {
    for (size_t j = 0; j < C; ++j) {
      stateVal = _state[i * C + j];
      output[i * C + j] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
    }
  }
}

void CNNArrayOpenMPv2::computeInitialOutput2()
{
  float stateVal;
  size_t M = _M;
  size_t N = _N;
  size_t C = _C;
  size_t r = _r;
  float* output = _output;
#pragma omp parallel for default(none) shared(M, N, r, C, output) private(stateVal)
  for (size_t i = 0; i < M; ++i) {
    for (size_t j = 0; j < N; ++j) {
      stateVal = _output[(i + r) * C + (j + r)];
      output[(i + r) * C + (j + r)] = (abs(stateVal + 1) - abs(stateVal - 1)) / 2.0f;
    }
  }
}

void CNNArrayOpenMPv2::copyOutput(float* out)
{
  for (size_t i = 0; i < _M; ++i) {
    for (size_t j = 0; j < _N; ++j) {
      out[i* _N + j] = _output[(i + _r) * _C + (j + _r)];
    }
  }
}
