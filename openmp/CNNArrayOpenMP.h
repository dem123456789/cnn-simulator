#ifndef CNN_ARRAY_OPENMP_H
#define CNN_ARRAY_OPENMP_H

#include "cnn/cnn.h"

class CNNArrayOpenMP : public CNN {
  private:
    float* _state;
    float* _output;

    size_t _R;
    size_t _C;

    
  public:
    CNNArrayOpenMP(size_t M, size_t N, size_t r);   
    ~CNNArrayOpenMP();

    void run(CNNGene* gene, CNNInput *input, unsigned int N, float dt);
    void run(CNNGene* gene, CNNInput *input, unsigned int N);
    void copyOutput(float* out);

  private:
    void computeNextState(float dt);
    void computeNextState();
    void computeInitialOutput();
    void computeOutput();
};

class CNNArrayOpenMPv2 : public CNN {
  private:
    float* _state;
    float* _output;
    float* _tmp_output;

    size_t _R;
    size_t _C;

    
  public:
    CNNArrayOpenMPv2(size_t M, size_t N, size_t r);   
    ~CNNArrayOpenMPv2();

    void run(CNNGene* gene, CNNInput *input, unsigned int N, float dt);
    void run(CNNGene* gene, CNNInput *input, unsigned int N);
    void copyOutput(float* out);

  private:
    void computeStateAndOutput(float dt);
    void computeStateAndOutput();
    void computeInitialOutput();
    void computeInitialOutput2();
};
#endif // CNN_ARRAY_OPENMP_H
