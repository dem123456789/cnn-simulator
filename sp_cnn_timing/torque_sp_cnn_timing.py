#! /usr/bin/python
import os
import shutil
import sys
import argparse

RESULTS_DIR = '/user/dmanatunga/cnn/timing-results'
SIM_DIR = '/user/dmanatunga/cnn/cnn-simulator'
SIM_TEST_DIR = '/user/dmanatunga/cnn/cnn-simulator/test_files'
SIM_CORRECT_OUTPUT_DIR = '/user/dmanatunga/cnn/cnn-simulator/test_outputs'

RUN_FILE = 'run.py'
RUN_TEST_DIR = 'test_files'
RUN_CORRECT_OUTPUT_DIR = 'correct_output'

COPY_LIST = [{'name': 'ini', 'is_dir': True}, {'name': 'lib', 'is_dir': True}, {'name': 'system.ini', 'is_dir': False}]

SP_CNN_SIM_DIR = '/user/dmanatunga/cnn/cnn-simulator/sp_cnn_timing/'
SP_CNN_BIN = 'sp-cnn-timing'

GENES = ['ConnectedComponent', 'CornerDetection', 'EdgeDetection', 'HoleFilling', 'RotationDetector', 'ShadowCreator']
#GENES=['HoleFilling']
TESTS = ['lowerA', 'capitalA', 'vertLine', 'circle', 'rect', 'filledSquare', 'zero', 'seven', 'eight', 'nine']
DIMS = [1024, 2048]
INTERVALS = [16, 32, 64, 128, 192, 256]
CNN_DIM = [128, 128]
NUM_CNN_UNITS = [1, 2, 4, 8]
RESULTS_FILENAME = 'results.out'



def parse_args():
    parser = argparse.ArgumentParser(description='Parse SP_CNN timing torque script arguments')
    parser.add_argument('run_name', help='Name of run output directory')
    parser.add_argument('run_type', help='Name of SP_CNN run type (either early-finish or fixed-interval)')
    parser.add_argument('--prefetch', dest='max_num_assigned_part', action='store_const', const=2, default=1, help='Whether to support prefetching')
    parser.add_argument('--part_trav_order', action='store', default='row-major', help='Partition traversal order')
    parser.add_argument('--cnn_freq', action='store', type=int, default=200, help='CNN_P frequency in MHz')
    parser.add_argument('--mem_size', action='store', type=int, default=2048, help='Memory size in MB')
    parser.add_argument('--mshr_size', action='store', type=int, default=64, help='Number of MSHR entries per CNN_P')
    parser.add_argument('--max_num_mem_req', action='store', type=int, default=1, help='Max number of memory requests CNN_P can issue per cycle')
    parser.add_argument('--bytes_per_mem_req', action='store', type=int, default=64, help='Number of bytes returned by memory request')
    parser.add_argument('--bytes_per_cnn_elem', action='store', type=int, default=1, help='Bytes per cnn element')
    parser.add_argument('--num_alus', action='store', type=int, default=1, help='Number of ALUs ber Processing Element')
    parser.add_argument('--cell_to_pe_ratio', action='store', type=int, default=32, help='Ratio of CNN cell to processing elements')
    parser.add_argument('--dram_mem_file', action='store', default='ini/DDR3_micron_16M_8B_x8_sg15.ini', help='DRAMSIM2 memory file')
    parser.add_argument('--dram_sys_file', action='store', default='system.ini', help='DRAMSIM2 system file')
    parser.add_argument('--dram_rel_dir', action='store', default='.', help='Relative directory of DRAMSIM2 files.')
    return parser.parse_args()


def get_sim_args(args):
    sim_args = {'gene': 'None',
                'run_type': 'None',
                'cnn_dim': [128, 128], 
                'interval': 128,
                'part_trav_order': 'row-major',
                'num_cnnP': 1,
                'dram_mem_file': 'ini/DDR3_micron_16M_8B_x8_sg15.ini',
                'dram_sys_file': 'system.ini',
                'dram_rel_dir': '.',
                'mem_size': 2048,
                'cnn_freq': 200,
                'mshr_size': 64,
                'max_num_mem_req': 1,
                'bytes_per_mem_req': 64,
                'bytes_per_cnn_elem': 1,
                'num_alus': 1,
                'cell_to_pe_ratio': 32,
                'max_num_assigned_part': 1}
    
    for key in sim_args:
        if hasattr(args, key):
            sim_args[key] = getattr(args, key)


    return sim_args


def run_sp_cnn(run_name, dim, sp_cnn_args, bench_base_dir, run_dir):
    file_name = os.path.join(bench_base_dir, RUN_FILE)
    gene = sp_cnn_args['gene']
    with open(file_name, 'w') as file:
        file.write('#!/usr/bin/python\n\n')
        file.write('import os\n')
        file.write('import glob\n')
        file.write('import sys\n\n')
        file.write('ppid = os.getppid()\n')
        bench = '%s_%d' % (gene, dim)
        file.write('test_dir = \'/tmp/cnnsim_\' + \'%s_\' + str(ppid) + \'/%s\'\n' % (run_name, bench))
        file.write('os.chdir(\'%s\')\n' % (bench_base_dir))
        file.write('os.system(\'uname -a\')\n')
        file.write('os.system(\'mkdir -p \%s\' % (test_dir))\n')
        file.write('os.system(\'cp -r %s/%s %%s/%s\' %% (test_dir))\n' % (run_dir, RUN_TEST_DIR, RUN_TEST_DIR))
        file.write('os.system(\'cp -r %s/%s %%s/%s\' %% (test_dir))\n' % (run_dir, RUN_CORRECT_OUTPUT_DIR, RUN_CORRECT_OUTPUT_DIR))
        for elem in COPY_LIST:
            if elem['is_dir']:
                file.write('os.system(\'cp -r %s/%s %%s/%s\' %% (test_dir))\n' % (run_dir, elem['name'], elem['name']))
            else:
                file.write('os.system(\'cp -p %s/%s %%s\' %% (test_dir))\n' % (run_dir, elem['name']))
        file.write('os.system(\'cp -p %s/%s %%s\' %% (test_dir))\n' % (run_dir, SP_CNN_BIN))
        file.write('os.chdir(\'%s\' % (test_dir))\n')
       
        file.write('print(\'Running Tests...\')\n')
        for test in TESTS:
            test_bench_dir = os.path.join(bench_base_dir, test)
            os.mkdir(test_bench_dir)
            test_name = '%s/%s_%dx%d.dlm' % (RUN_TEST_DIR,test, dim, dim)
            correct_output_name = '%s/%s_%s_%dx%d.dlm' % (RUN_CORRECT_OUTPUT_DIR, gene, test, dim, dim)
            file.write('os.system(\'./%s %s %%s/%s %s %d %d %d %s %%s/%s --num_cnnP %d --dram_mem_file %s --dram_sys_file %s --dram_rel_dir %s --mem_size %d --cnn_freq %d --mshr_size %d --max_num_mem_req %d --bytes_per_mem_req %d --bytes_per_cnn_elem %d --num_alus %d --cell_to_pe_ratio %d --max_num_assigned_part %d >> %s\' %% (test_dir, test_dir))\n' %
                                    (SP_CNN_BIN,
                                     sp_cnn_args['gene'], 
                                     test_name, 
                                     sp_cnn_args['run_type'],
                                     sp_cnn_args['cnn_dim'][0], sp_cnn_args['cnn_dim'][1],
                                     sp_cnn_args['interval'],
                                     sp_cnn_args['part_trav_order'], 
                                     correct_output_name,
                                     sp_cnn_args['num_cnnP'],
                                     sp_cnn_args['dram_mem_file'],
                                     sp_cnn_args['dram_sys_file'],
                                     sp_cnn_args['dram_rel_dir'],
                                     sp_cnn_args['mem_size'],
                                     sp_cnn_args['cnn_freq'],
                                     sp_cnn_args['mshr_size'],
                                     sp_cnn_args['max_num_mem_req'],
                                     sp_cnn_args['bytes_per_mem_req'],
                                     sp_cnn_args['bytes_per_cnn_elem'],
                                     sp_cnn_args['num_alus'],
                                     sp_cnn_args['cell_to_pe_ratio'],
                                     sp_cnn_args['max_num_assigned_part'],
                                     RESULTS_FILENAME))
            file.write('os.system(\'mv *.log %s/\')\n' % (test_bench_dir))
            file.write('os.system(\'mv results %s/\')\n' % (test_bench_dir))
            file.write('os.system(\'mv *.csv %s/\')\n' % (test_bench_dir))
            file.write('os.system(\'mv *.cfg %s/\')\n' % (test_bench_dir))

        file.write('print(\'Tests Completed...\')\n')
        file.write('os.system(\'mv %s %s/%s\')\n' % (RESULTS_FILENAME, bench_base_dir, RESULTS_FILENAME))
        file.write('os.system(\'rm -rf %s\' % (test_dir))\n')
        file.close()

    os.system('chmod +x %s' % (file_name))
   
    # qsub command
    cmd = []
    cmd += ['qsub']
    cmd += [RUN_FILE]
    cmd += ['-V -m n']
    cmd += ['-o', '%s/qsub.stdout' % (bench_base_dir)]
    cmd += ['-e', '%s/qsub.stderr' % (bench_base_dir)]
    cmd += ['-q', 'pool1']
    cmd += ['-N', '%s_%s' % (run_name, bench)]
    cmd += ['-l', 'nodes=1:ppn=1']

    cwd = os.getcwd()
    os.chdir('%s' % (bench_base_dir))
    os.system('/bin/echo \'%s\' > %s/TORQUE_RUN_CMD' % (' '.join(cmd), bench_base_dir))
    os.system('chmod +x %s/TORQUE_RUN_CMD' % bench_base_dir)
    os.system('%s | tee %s/JOB_ID' % (' '.join(cmd), bench_base_dir))
    os.chdir(cwd)

def cnnsim(args):
    run_name = args.run_name
    cur_dir = os.getcwd()
    run_dir = os.path.join(RESULTS_DIR, run_name)

    if os.path.exists(run_dir):
        print("Erasing current directory...");
        shutil.rmtree(run_dir)
      
    os.makedirs(run_dir)
    os.makedirs(os.path.join(run_dir, RUN_TEST_DIR))
    os.makedirs(os.path.join(run_dir, RUN_CORRECT_OUTPUT_DIR))

    print('Copying test and output files...')
    for test in TESTS:
        for dim in DIMS:
            test_name = '%s_%dx%d.dlm' % (test, dim, dim)
            shutil.copyfile(os.path.join(SIM_TEST_DIR, test_name), os.path.join(run_dir, RUN_TEST_DIR, test_name))

            for gene in GENES:
                correct_output_name = '%s_%s' % (gene, test_name)
                shutil.copyfile(os.path.join(SIM_CORRECT_OUTPUT_DIR, gene, correct_output_name),
                                os.path.join(run_dir, RUN_CORRECT_OUTPUT_DIR, correct_output_name))
                                
    shutil.copy2(os.path.join(SP_CNN_SIM_DIR, SP_CNN_BIN), os.path.join(run_dir, SP_CNN_BIN))
    
    for elem in COPY_LIST:
        if elem['is_dir']:
            shutil.copytree(os.path.join(SP_CNN_SIM_DIR, elem['name']), os.path.join(run_dir, elem['name']))
        else:
            shutil.copy2(os.path.join(SP_CNN_SIM_DIR, elem['name']), os.path.join(run_dir, elem['name']))


    print('Generating TORQUE test files...')
    for gene in GENES:
        for dim in DIMS:
            for numUnits in NUM_CNN_UNITS:
                for interval in INTERVALS:
                    bench_dir = os.path.join(run_dir, '%s_%d_%d_%d' % (gene, dim, numUnits, interval))
                    os.mkdir(bench_dir)
                    sim_args = get_sim_args(args)
                    sim_args['gene'] = gene
                    sim_args['num_cnnP'] = numUnits
                    sim_args['cnn_dim'] = CNN_DIM
                    sim_args['interval'] = interval
                    run_sp_cnn(run_name, dim, sim_args, bench_dir, run_dir)

if __name__ == "__main__":
    args = parse_args()
    cnnsim(args)
