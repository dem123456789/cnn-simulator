#ifndef CNN_SIM_GPU_H
#define CNN_SIM_GPU_H

#define BLOCK_SIZE 32

#ifdef __cplusplus
extern "C" {
#endif
  void cnn_gpu_init(size_t M, size_t N, size_t r, float** d_state, float** d_output, float** d_u, float** d_templates);
  void cnn_gpu_init2(size_t M, size_t N, size_t r, float** d_state, float** d_output, float** d_output2, float** d_u, float** d_templates);

  void cnn_gpu_finish(float* d_state, float* d_output, float* d_u, float* d_templates);
  void cnn_gpu_finish2(float* d_state, float* d_output, float* d_output2, float* d_u, float* d_templates);

  void cnn_gpu_run(size_t M, size_t N, size_t r, float* h_templates, float* d_templates, float z, 
            float* h_state, float* d_state, float* d_output, float* h_u, float* d_u,
            unsigned int N_iters, float dt, float* h_output);

  void cnn_gpu_run2(size_t M, size_t N, size_t r, float* h_templates, float* d_templates, float z, 
            float* h_state, float* d_state, float* d_output, float* d_output2, float* h_u, float* d_u,
            unsigned int N_iters, float dt, float* h_output);

  void diffeq_cnn_gpu_run(size_t M, size_t N, size_t r, float* h_templates, float* d_templates, float z, 
            float* h_state, float* d_state, float* d_output, float* h_u, float* d_u,
            unsigned int N_iters, float* h_output);
  
  void diffeq_cnn_gpu_run2(size_t M, size_t N, size_t r, float* h_templates, float* d_templates, float z, 
            float* h_state, float* d_state, float* d_output, float* d_output2, float* h_u, float* d_u,
            unsigned int N_iters, float* h_output);

#ifdef __cplusplus
}
#endif
#endif // CNN_SIM_GPU_H
