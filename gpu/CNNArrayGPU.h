#ifndef CNN_ARRAY_GPU_H
#define CNN_ARRAY_GPU_H

#include "cnn/cnn.h"
#include <assert.h>

class CNNArrayGPU : public CNN {
  private:
    float* _d_state;
    float* _d_output;
    float* _d_u;
    float* _d_templates;

    float* _h_output;
    float* _h_templates;

    size_t _R;
    size_t _C;
    
  public:
    CNNArrayGPU(size_t M, size_t N, size_t r);   
    ~CNNArrayGPU();

    void run(CNNGene* gene, CNNInput *input, unsigned int N_iters, float dt);
    void run(CNNGene* gene, CNNInput *input, unsigned int N_iters);
    void copyOutput(float* out);

    unsigned int convergenceRun(CNNGene* gene, CNNInput *input, float dt) { assert(false); }
    unsigned int convergenceRun(CNNGene* gene, CNNInput *input)  { assert(false); }
    std::vector<long long int> outputAnalysisRun(CNNGene* gene, CNNInput* input, float* idealOutput) { assert(false); }
};


class CNNArrayGPUv2 : public CNN {
  private:
    float* _d_state;
    float* _d_output;
    float* _d_output2;
    float* _d_u;
    float* _d_templates;

    float* _h_output;
    float* _h_templates;

    size_t _R;
    size_t _C;
    
  public:
    CNNArrayGPUv2(size_t M, size_t N, size_t r);   
    ~CNNArrayGPUv2();

    void run(CNNGene* gene, CNNInput *input, unsigned int N_iters, float dt);
    void run(CNNGene* gene, CNNInput *input, unsigned int N_iters);
    void copyOutput(float* out);

    unsigned int convergenceRun(CNNGene* gene, CNNInput *input, float dt) { assert(false); }
    unsigned int convergenceRun(CNNGene* gene, CNNInput *input)  { assert(false); }
    std::vector<long long int> outputAnalysisRun(CNNGene* gene, CNNInput* input, float* idealOutput) { assert(false); }
};

#endif
